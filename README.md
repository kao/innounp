## Modifications ##
This innounp version is modified by kao. Original version is available at http://innounp.sourceforge.net

* * *

### innounp, the Inno Setup Unpacker ###
Version 0.50
Supports Inno Setup versions 2.0.7 through 6.1.2

[Inno Setup](http://www.jrsoftware.org/isinfo.php) is a popular program for making software installations. Unfortunately, there is no official unpacker - the only method of getting the files out of the self-extracting executable is to run it. One piece of software that addresses this issue is Sergei Wanin's [InstallExplorer](http://plugring.farmanager.com/downld/files/instexpl_v0.3.rar), a plug-in for the [FAR Manager](http://farmanager.com/) that unpacks several types of installations, including Inno Setup (IS). But since it is not updated in a timely fashion, and so does not support the latest IS most of the time, this program was born. The advantages over InstallExplorer are:

Innounp is open source and based on IS source. Therefore, it is more likely to support future IS versions.
It recovers portions of the installation script (.iss file), including the registry changes and the compiled Innerfuse/RemObjects Pascal Script, if available.
If you want to report a bug, request a feature, or discuss anything else related to the program, please write to the [original forum](http://sourceforge.net/projects/innounp/forums/forum/353235).

### Copyrights and licensing ###
Copyright © 2004-2020 QuickeneR, 2009-2020 Ariman
This program is licensed under the terms of the GNU General Public License (GPL). A copy of the license is included with the source files. 
If you distribute innounp on the WWW, please put a link to its home page, http://innounp.sourceforge.net

Over 90% of code is ripped from Inno Setup which is Copyright © 1997-2010 Jordan Russell. All rights reserved.
Portions Copyright © 2000-2006 Martijn Laan. All rights reserved.
See http://www.jrsoftware.org for details.

Contains zlib code, Copyright © 1995-2005 Jean-loup Gailly and Mark Adler.
Contains bzip2 code, Copyright © 1996-2009 Julian R Seward. All rights reserved.
Contains LZMA code, Copyright © 1999-2009 Igor Pavlov.

Innerfuse Pascal Script is Copyright © 2000-2004 by Carlo Kok, Innerfuse.
StripReloc is Copyright © 1999-2005 Jordan Russell, www.jrsoftware.org